package api

import (
	"bytes"
	"encoding/json"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"

	"github.com/google/jsonapi"
	"github.com/julienschmidt/httprouter"
	"gitlab.com/openvm/openvm/model"
	"golang.org/x/crypto/bcrypt"
)

func TestUserLogin(t *testing.T) {
	pass, err := bcrypt.GenerateFromPassword([]byte("hunterhunter"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user := model.User{
		Name:         strconv.Itoa(rand.Int()),
		PasswordHash: string(pass),
	}
	if err := testDB.Insert(&user); err != nil {
		t.Fatal(err)
	}

	login := LoginRequest{
		Name:     user.Name,
		Password: "hunterhunter",
	}
	payload, _ := jsonapi.Marshal(&login)
	buf, _ := json.Marshal(payload)

	r, err := http.NewRequest("POST", "/user/login", bytes.NewBuffer(buf))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	router := httprouter.New()
	router.Handle("POST", "/user/login", testHandler.UserLogin)
	router.ServeHTTP(rr, r)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}
}

func TestUserLoginInvalidPassword(t *testing.T) {
	pass, err := bcrypt.GenerateFromPassword([]byte("hunterhunter"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user := model.User{
		Name:         strconv.Itoa(rand.Int()),
		PasswordHash: string(pass),
	}
	if err := testDB.Insert(&user); err != nil {
		t.Fatal(err)
	}

	login := LoginRequest{
		Name:     user.Name,
		Password: "nope",
	}
	payload, _ := jsonapi.Marshal(&login)
	buf, _ := json.Marshal(payload)

	r, err := http.NewRequest("POST", "/user/login", bytes.NewBuffer(buf))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	router := httprouter.New()
	router.Handle("POST", "/user/login", testHandler.UserLogin)
	router.ServeHTTP(rr, r)

	if status := rr.Code; status != http.StatusUnauthorized {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusUnauthorized)
	}
}
