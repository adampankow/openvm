package api

import (
	"github.com/go-pg/pg"
	"gitlab.com/openvm/openvm/model"
)

// MachineInfo return info about a machine
func (h *Handler) MachineInfo(r Request) Response {
	if !r.Authenticated {
		return Unauthorized
	}
	name := r.Params.ByName("name")

	var machine model.Machine
	if err := h.DB.Model(&machine).Where("name = ?", name).Select(); err != nil {
		if err != pg.ErrNoRows {
			return Response{InternalError: err}
		}
		return NotFound
	}

	if machine.UserId != r.User {
		return AccessDenied
	}

	status, stats, err := h.Container.Get(machine.Id)
	if err != nil {
		return Response{InternalError: err}
	}

	machine.Status = *status
	machine.Stats = *stats

	return Response{Resource: machine}
}
