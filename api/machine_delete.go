package api

import (
	"strconv"

	"github.com/go-pg/pg"
	"gitlab.com/openvm/openvm/model"
)

// MachineDelete deletes a machine
func (h *Handler) MachineDelete(r Request) Response {
	if !r.Authenticated {
		return Unauthorized
	}

	id, err := strconv.ParseInt(r.Params.ByName("id"), 10, 64)
	if err != nil {
		return BadRequest
	}

	machine := model.Machine{Id: id}
	if err := h.DB.Select(&machine); err != nil {
		if err != pg.ErrNoRows {
			return Response{InternalError: err}
		}
		return NotFound
	}

	if machine.UserId != r.User {
		return AccessDenied
	}

	if err := h.Container.Delete(machine.Id); err != nil {
		return Response{InternalError: err}
	}

	if err := h.DB.Delete(&machine); err != nil {
		return Response{InternalError: err}
	}

	return Response{Status: 204}
}
