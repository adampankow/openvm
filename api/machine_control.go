package api

import (
	"net/http"
	"strconv"

	"github.com/gorilla/websocket"
	"gitlab.com/openvm/openvm/container"
)

// MachineControl exposes the control socket
func (h *Handler) MachineControl(r Request) Response {
	if !r.Authenticated {
		return Unauthorized
	}

	id, err := strconv.ParseInt(r.Params.ByName("id"), 10, 64)
	if err != nil {
		return BadRequest
	}

	session, ok := sessions[id]
	if !ok {
		return NotFound
	}

	if session.user != r.User {
		return AccessDenied
	}

	upgrader := websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool {
			origin := r.Header.Get("Origin")
			for _, o := range h.AllowOrigins {
				if o == origin || o == "*" {
					return true
				}
			}
			return false
		},
	}

	conn, err := upgrader.Upgrade(r.Writer, r.Request, nil)
	if err != nil {
		return Response{InternalError: err}
	}
	defer conn.Close()

	for {
		var msg container.SessionControlMessage
		if err := conn.ReadJSON(&msg); err != nil {
			conn.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseGoingAway, "malformed message"))
			break
		}

		session.control <- msg
	}

	return Response{Status: http.StatusOK}
}
