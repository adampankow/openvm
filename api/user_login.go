package api

import (
	"net/http"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/go-pg/pg"
	"github.com/google/jsonapi"
	"gitlab.com/openvm/openvm/model"
	"golang.org/x/crypto/bcrypt"
)

// LoginDenied is returned when username or password are invalid
var LoginDenied = Response{
	Status: http.StatusUnauthorized,
	Error: &jsonapi.ErrorObject{
		Code:   "login_denied",
		Title:  "Login request denied",
		Detail: "Invalid username or password",
	},
}

// UserLogin takes user & password and returns a jwt token
func (h *Handler) UserLogin(r Request) Response {
	login := new(UserLoginPost)
	if err := jsonapi.UnmarshalPayload(r.Body, login); err != nil {
		return BadRequest
	}

	var user model.User
	if err := h.DB.Model(&user).Where("name = ?", login.Name).Select(); err != nil {
		if err != pg.ErrNoRows {
			return Response{InternalError: err}
		}
		return LoginDenied
	}

	if bcrypt.CompareHashAndPassword([]byte(user.PasswordHash), []byte(login.Password)) != nil {
		return LoginDenied
	}

	now := time.Now()

	claims := jwt.MapClaims{
		"id":   user.Id,
		"name": user.Name,
		"exp":  now.Add(time.Hour * 48).Unix(),
		"nbf":  now.Truncate(time.Hour).Unix(),
		"iat":  now.Unix(),
	}

	for _, claim := range login.Claims {
		switch claim {
		case "email":
			claims["email"] = user.Email
		}
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(h.JWTSecret)
	if err != nil {
		return Response{InternalError: err}
	}

	return Response{Resource: &UserLogin{user.Id, tokenString}}
}
