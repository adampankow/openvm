package api

import (
	"net/http"
	"time"

	"github.com/asaskevich/govalidator"
	"github.com/google/jsonapi"
	"gitlab.com/openvm/openvm/model"
	"golang.org/x/crypto/bcrypt"
)

var (
	// NameConflict is returned when the user name is taken
	NameConflict = Response{
		Status: http.StatusConflict,
		Error: &jsonapi.ErrorObject{
			Code:   "name_conflict",
			Title:  "Account with this name exists",
			Detail: "Please choose a different username",
		},
	}
	// EmailConflict is returned when the user email is taken
	EmailConflict = Response{
		Status: http.StatusConflict,
		Error: &jsonapi.ErrorObject{
			Code:   "email_conflict",
			Title:  "Account with this email exists",
			Detail: "Please choose a different email",
		},
	}
)

// UserCreate creates a new user
func (h *Handler) UserCreate(r Request) Response {
	signup := new(UserPost)
	if err := jsonapi.UnmarshalPayload(r.Body, signup); err != nil {
		return BadRequest
	}

	_, err := govalidator.ValidateStruct(signup)
	if err != nil {
		e := ValidationFailed
		e.Error.Detail = err.Error()
		return e
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(signup.Password), bcrypt.DefaultCost)
	if err != nil {
		return Response{InternalError: err}
	}

	user := model.User{
		Email:        signup.Email,
		Name:         signup.Name,
		PasswordHash: string(hash),
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
	}

	if count, err := h.DB.Model(&model.User{}).Where("name = ?", signup.Name).Count(); err != nil {
		return Response{InternalError: err}
	} else if count > 0 {
		return NameConflict
	}

	if count, err := h.DB.Model(&model.User{}).Where("email = ?", signup.Email).Count(); err != nil {
		return Response{InternalError: err}
	} else if count > 0 {
		return EmailConflict
	}

	if err := h.DB.Insert(&user); err != nil {
		return Response{InternalError: err}
	}

	return Response{Resource: &user, Status: http.StatusCreated}
}
