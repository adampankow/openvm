package api

import (
	"gitlab.com/openvm/openvm/model"
)

// MachineList lists all machines of a user
func (h *Handler) MachineList(r Request) Response {
	if !r.Authenticated {
		return Unauthorized
	}

	var machines []*model.Machine
	if err := h.DB.Model(&machines).Where("user_id = ?", r.User).Select(); err != nil {
		return Response{InternalError: err}
	}

	for _, machine := range machines {
		status, stats, err := h.Container.Get(machine.Id)
		if err != nil {
			return Response{InternalError: err}
		}
		machine.Status = *status
		machine.Stats = *stats
	}

	return Response{Resource: machines}
}
