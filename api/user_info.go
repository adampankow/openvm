package api

import (
	"github.com/go-pg/pg"
	"gitlab.com/openvm/openvm/model"
)

// UserInfo returns information about a user
func (h *Handler) UserInfo(r Request) Response {
	if !r.Authenticated {
		return Unauthorized
	}

	user := model.User{Id: r.User}
	if err := h.DB.Select(&user); err != nil {
		if err != pg.ErrNoRows {
			return Response{InternalError: err}
		}
		return NotFound
	}

	return Response{Resource: &user}
}
