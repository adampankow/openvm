package api

import (
	"fmt"
	"io"
	"net/http"

	"github.com/google/jsonapi"
	jwtmiddleware "github.com/jgillich/jwt-middleware"
	"github.com/julienschmidt/httprouter"
	"go.uber.org/zap"
)

type Handle func(Request) Response

type Request struct {
	Writer        http.ResponseWriter
	Request       *http.Request
	Body          io.ReadCloser
	Params        httprouter.Params
	Authenticated bool
	User          int64
}

type Response struct {
	Status        int
	Resource      interface{}
	Error         *jsonapi.ErrorObject
	InternalError error
}

type Router struct {
	router *httprouter.Router
}

func NewRouter() *Router {
	return &Router{httprouter.New()}
}

func (r *Router) GET(path string, handle Handle) {
	r.Route("GET", path, handle)
}

func (r *Router) POST(path string, handle Handle) {
	r.Route("POST", path, handle)
}

func (r *Router) PUT(path string, handle Handle) {
	r.Route("PUT", path, handle)
}

func (r *Router) PATCH(path string, handle Handle) {
	r.Route("PATCH", path, handle)
}

func (r *Router) DELETE(path string, handle Handle) {
	r.Route("DELETE", path, handle)
}

func (r *Router) Route(method, path string, handle Handle) {
	r.router.Handle(method, path, func(w http.ResponseWriter, r *http.Request, params httprouter.Params) {

		var user int64
		authenticated := false

		claims, err := jwtmiddleware.ContextClaims(r)
		if err == nil {
			authenticated = true
			user = int64(claims["id"].(float64))
		}

		req := Request{
			Writer:        w,
			Request:       r,
			Body:          r.Body,
			Params:        params,
			Authenticated: authenticated,
			User:          user,
		}

		res := handle(req)
		var marshalError error

		if res.InternalError != nil {
			w.Header().Set("Content-Type", jsonapi.MediaType)
			logger.Error(fmt.Sprintf("Error at route '%s %s'", method, path), zap.Error(res.InternalError))
			w.WriteHeader(http.StatusInternalServerError)
			marshalError = jsonapi.MarshalErrors(w, []*jsonapi.ErrorObject{InternalServerError})
		} else if res.Error != nil {
			w.Header().Set("Content-Type", jsonapi.MediaType)
			if res.Status > http.StatusBadRequest {
				w.WriteHeader(res.Status)
			} else {
				w.WriteHeader(http.StatusBadRequest)
			}
			marshalError = jsonapi.MarshalErrors(w, []*jsonapi.ErrorObject{res.Error})
		} else if res.Resource != nil {
			w.Header().Set("Content-Type", jsonapi.MediaType)
			if res.Status > http.StatusOK {
				w.WriteHeader(res.Status)
			} else {
				w.WriteHeader(http.StatusOK)
			}
			marshalError = jsonapi.MarshalPayload(w, res.Resource)
		} else {
			if res.Status != 0 {
				w.WriteHeader(res.Status)
			} else {
				logger.Error(fmt.Sprintf("Route '%s %s' did not return valid response", method, path))
			}
		}

		if marshalError != nil {
			logger.Error("error marshalling response", zap.Error(marshalError))
		}
	})
}

func (r *Router) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	r.router.ServeHTTP(writer, request)
}
