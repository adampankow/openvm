package api

import (
	"testing"

	"gitlab.com/openvm/openvm/model"
	"gitlab.com/openvm/openvm/scheduler"

	"os"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
)

var (
	testToken = &jwt.Token{Claims: jwt.MapClaims{
		"id": float64(1),
	}}
	testScheduler, _ = scheduler.NewMockScheduler(nil)
	mockScheduler    = testScheduler.(*scheduler.MockScheduler)
	testDB           *pg.DB
	testHandler      *Handler
)

func TestMain(m *testing.M) {
	testDB = pg.Connect(&pg.Options{
		Addr:     os.Getenv("POSTGRES_ADDR"),
		User:     os.Getenv("POSTGRES_USER"),
		Password: os.Getenv("POSTGRES_PASSWORD"),
		Database: os.Getenv("POSTGRES_DB"),
	})

	if err := testDB.CreateTable(&model.Machine{}, &orm.CreateTableOptions{
		Temp: true,
	}); err != nil {
		panic(err)
	}

	if err := testDB.CreateTable(&model.User{}, &orm.CreateTableOptions{
		Temp: true,
	}); err != nil {
		panic(err)
	}

	testHandler = &Handler{
		DB:           testDB,
		Scheduler:    testScheduler,
		AllowOrigins: []string{},
		JWTSecret:    []byte("foobar"),
	}

	retCode := m.Run()

	testDB.Close()
	os.Exit(retCode)
}
