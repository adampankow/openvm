package api

import (
	"net/http"

	"gitlab.com/openvm/openvm/container"
	"go.uber.org/zap"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/go-pg/pg"
	"github.com/jgillich/jwt-middleware"
	"github.com/rs/cors"
	"github.com/urfave/negroni"
)

var logger, _ = zap.NewDevelopment()

// Handler stores common types needed by the api
type Handler struct {
	DB           *pg.DB
	Container    *container.Container
	JWTSecret    []byte
	AllowOrigins []string
}

// Serve sets up the http server and listens on the provided addr
func (h *Handler) Serve(addr string) error {

	router := NewRouter()
	router.GET("/machines", h.MachineList)
	router.POST("/machines", h.MachineCreate)
	router.DELETE("/machines/:id", h.MachineDelete)
	router.POST("/machines/:id", h.MachineUpdate)
	router.GET("/machines/:id", h.MachineInfo)
	router.GET("/machines/:id/io", h.MachineIO)
	router.GET("/machines/:id/control", h.MachineControl)
	router.POST("/user", h.UserCreate)
	router.POST("/user/login", h.UserLogin)
	router.GET("/user", h.UserInfo)
	router.GET("/images", h.ImageList)

	middleware := negroni.New()

	recovery := negroni.NewRecovery()
	middleware.Use(recovery)

	middleware.Use(negroni.NewLogger())

	middleware.Use(cors.New(cors.Options{
		AllowedOrigins: h.AllowOrigins,
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "PATCH"},
		AllowedHeaders: []string{"Authorization", "Content-Type"},
	}))

	jwt := jwtmiddleware.New(jwtmiddleware.Options{
		/*ErrorHandler: func(w http.ResponseWriter, r *http.Request, err string) {
			WriteError(w, http.StatusUnauthorized, &jsonapi.ErrorObject{
				Code:   "unauthorized",
				Title:  "Unauthorized",
				Detail: err,
			})
		},*/
		ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
			return h.JWTSecret, nil
		},
		SigningMethod:       jwt.SigningMethodHS256,
		CredentialsOptional: true,
		Extractor: func(r *http.Request) (string, error) {
			token, err := jwtmiddleware.FromAuthHeader(r)

			if token == "" {
				return r.URL.Query().Get("access_token"), nil
			}

			return token, err
		},
	})
	middleware.Use(negroni.HandlerFunc(jwt.HandlerWithNext))

	middleware.UseHandler(router)

	return http.ListenAndServe(addr, middleware)
}
