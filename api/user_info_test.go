package api

import (
	"context"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/google/jsonapi"
	jwtmiddleware "github.com/jgillich/jwt-middleware"
	"github.com/julienschmidt/httprouter"

	"gitlab.com/openvm/openvm/model"
)

func TestUserInfo(t *testing.T) {
	user := model.User{
		Name: strconv.Itoa(rand.Int()),
	}
	if err := testDB.Insert(&user); err != nil {
		t.Fatal(err)
	}

	r, err := http.NewRequest("GET", "/user", nil)
	if err != nil {
		t.Fatal(err)
	}
	token := &jwt.Token{Claims: jwt.MapClaims{"id": user.ID}}
	*r = *r.WithContext(context.WithValue(r.Context(), jwtmiddleware.TokenContextKey{}, token))

	rr := httptest.NewRecorder()
	router := httprouter.New()
	router.Handle("GET", "/user", testHandler.UserInfo)
	router.ServeHTTP(rr, r)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}
	var payload model.User
	if err := jsonapi.UnmarshalPayload(rr.Body, &payload); err != nil {
		t.Fatal(err)
	}

	if payload.ID != user.ID {
		t.Errorf("returned user id does not match: got %v want %v", payload.ID, user.ID)
	}
}
