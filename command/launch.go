package command

import (
	"flag"
	"fmt"
	"strings"

	"github.com/mitchellh/cli"
	"gitlab.com/openvm/openvm/client"
	"gitlab.com/openvm/openvm/model"
)

type LaunchCommand struct {
	Ui cli.Ui
}

func (c LaunchCommand) Run(args []string) int {
	flags := flag.NewFlagSet("launch", flag.PanicOnError)
	flags.Usage = func() { c.Ui.Output(c.Help()) }
	if err := flags.Parse(args); err != nil {
		return 1
	}

	args = flags.Args()
	if len(args) != 1 || len(args[0]) == 0 {
		c.Ui.Error("launch expects one argument")
		flags.Usage()
		return 1
	}

	var name = flags.String("name", "", "Machine name")

	image := args[0]

	client := client.New("TODO", "TODO")
	machine, err := client.MachineCreate(&model.Machine{Image: image, Name: *name, Driver: "lxd"})
	if err != nil {
		c.Ui.Error(fmt.Sprintf("Error creating machine: %s", err))
		return 1
	}

	fmt.Printf("Machine '%v' was created.", machine.Name)
	return 0
}

func (c LaunchCommand) Help() string {
	helpText := `
Usage: openvm launch [options] image

  Launch a machine

  General Options:

    -name=<name>          Name to assign to the machine.
	`
	return strings.TrimSpace(helpText)
}

func (c LaunchCommand) Synopsis() string {
	return "Launch a machine"
}
