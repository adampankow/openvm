package command

import (
	"flag"
	"fmt"
	"strings"

	"github.com/go-pg/pg"
	"github.com/mitchellh/cli"
	"gitlab.com/openvm/openvm/api"
	"gitlab.com/openvm/openvm/config"
	"gitlab.com/openvm/openvm/container"
)

type ServerCommand struct {
	Ui cli.Ui
}

func (c ServerCommand) Run(args []string) int {
	flags := flag.NewFlagSet("server", flag.PanicOnError)
	flags.Usage = func() { c.Ui.Output(c.Help()) }

	var configPath = flags.String("config", "config.hcl", "config file path")

	conf, err := config.ParseFile(*configPath)
	if err != nil {
		fmt.Println(err)
		return 1
	}

	db := pg.Connect(&pg.Options{
		Addr:     conf.Postgres.Address,
		User:     conf.Postgres.Username,
		Password: conf.Postgres.Password,
		Database: conf.Postgres.Database,
	})

	_, err = db.Exec("SELECT")
	if err != nil {
		fmt.Println(err)
		return 1
	}

	container, err := container.New(conf.Lxd)
	if err != nil {
		fmt.Println(err)
		return 1
	}

	handler := api.Handler{
		DB:           db,
		JWTSecret:    []byte(conf.JWT.Secret),
		Container:    container,
		AllowOrigins: conf.AllowOrigins,
	}

	fmt.Printf("Serving on '%v'\n", conf.Address)

	if err := handler.Serve(conf.Address); err != nil {
		fmt.Println(err)
		return 1
	}
	return 0
}

func (c ServerCommand) Help() string {
	helpText := `
  Usage: openvm server [options]

    Start a OpenVM server

    This command starts a OpenVM server that responds to API requests.


  General Options:

    -config=<path>          Path to the configuration file.
	`
	return strings.TrimSpace(helpText)
}

func (c ServerCommand) Synopsis() string {
	return "Start a OpenVM server"
}
