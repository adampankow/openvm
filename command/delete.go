package command

import (
	"flag"
	"fmt"
	"strings"

	"github.com/mitchellh/cli"
	"gitlab.com/openvm/openvm/client"
)

type DeleteCommand struct {
	Ui cli.Ui
}

func (c DeleteCommand) Run(args []string) int {
	flags := flag.NewFlagSet("delete", flag.PanicOnError)
	flags.Usage = func() { c.Ui.Output(c.Help()) }
	if err := flags.Parse(args); err != nil {
		return 1
	}

	args = flags.Args()
	if len(args) != 1 || len(args[0]) == 0 {
		c.Ui.Error("delete expects one argument")
		flags.Usage()
		return 1
	}

	name := args[0]

	client := client.New("TODO", "TODO")
	if err := client.MachineDelete(name); err != nil {
		c.Ui.Error(fmt.Sprintf(
			"Error deleting machine: %s", err))
		return 1
	}

	fmt.Printf("Machine '%v' was deleted.", name)

	return 0
}

func (c DeleteCommand) Help() string {
	helpText := `
Usage: openvm delete [options] name

  Delete a machine.
	`
	return strings.TrimSpace(helpText)
}

func (c DeleteCommand) Synopsis() string {
	return "Delete a machine"
}
