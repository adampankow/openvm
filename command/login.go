package command

import (
	"flag"
	"fmt"
	"strings"

	"github.com/mitchellh/cli"
	"gitlab.com/openvm/openvm/client"
)

type LoginCommand struct {
	Ui cli.Ui
}

func (c LoginCommand) Run(args []string) int {
	flags := flag.NewFlagSet("list", flag.PanicOnError)
	flags.Usage = func() { c.Ui.Output(c.Help()) }
	if err := flags.Parse(args); err != nil {
		return 1
	}

	args = flags.Args()
	if len(args) != 1 || len(args[0]) == 0 {
		c.Ui.Error("login expects one argument")
		flags.Usage()
		return 1
	}

	server := args[0]

	username, err := c.Ui.Ask("Username")
	if err != nil {
		c.Ui.Error(fmt.Sprintf("Error reading username: %s", err))
		return 1
	}

	password, err := c.Ui.AskSecret("Password")
	if err != nil {
		c.Ui.Error(fmt.Sprintf("Error reading password: %s", err))
		return 1
	}
	client := client.New(server, "")
	token, err := client.Login(username, password)

	if err != nil {
		c.Ui.Error(fmt.Sprintf("Error logging in: %s", err))
		return 1
	}

	// TODO store token in ~/.config/openvm/servername ?
	fmt.Println(token)

	fmt.Printf("Logged in as '%v'.", username)

	return 0
}

func (c LoginCommand) Help() string {
	helpText := `
Usage: openvm login [options] server

  Aquire token to access server.
	`
	return strings.TrimSpace(helpText)
}

func (c LoginCommand) Synopsis() string {
	return "Aquire token to access server"
}
