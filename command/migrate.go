package command

import (
	"flag"
	"fmt"
	"strings"

	"github.com/go-pg/migrations"
	"github.com/go-pg/pg"
	"github.com/mitchellh/cli"
	"gitlab.com/openvm/openvm/config"
	// Required for migrations to be picked up
	_ "gitlab.com/openvm/openvm/model/migrations"
)

// MigrateCommand applies migrations
type MigrateCommand struct {
	Ui cli.Ui
}

func (c MigrateCommand) Run(args []string) int {
	var configPath = flag.String("config", "config.hcl", "config file path")

	cfg, err := config.ParseFile(*configPath)
	if err != nil {
		fmt.Println(err)
		return 1
	}

	db := pg.Connect(&pg.Options{
		Addr:     cfg.Postgres.Address,
		User:     cfg.Postgres.Username,
		Password: cfg.Postgres.Password,
		Database: cfg.Postgres.Database,
	})

	old, new, err := migrations.Run(db, args...)
	if err != nil {
		fmt.Println(err)
		return 1
	}

	fmt.Printf("Migrated: %v -> %v", old, new)

	return 0
}

func (c MigrateCommand) Help() string {
	helpText := `
Usage: openvm migrate [options] command
`
	return strings.TrimSpace(helpText)
}

func (c MigrateCommand) Synopsis() string {
	return "Migrate database"
}
