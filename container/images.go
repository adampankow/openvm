package container

import "github.com/lxc/lxd/shared/api"

func (container *Container) Images() ([]api.Image, error) {
	return container.imageServer.GetImages()
}
