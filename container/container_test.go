package container

import (
	"fmt"
	"testing"

	"code.cloudfoundry.org/bytefmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/openvm/openvm/config"
	"gitlab.com/openvm/openvm/model"
)

func TestContainer(t *testing.T) {
	id := int64(123)

	options := config.DriverOptions{
		RemoteURL: "unix://",
	}

	driver, err := NewDriver(options)
	assert.NoError(t, err)

	props := model.MachineProperties{
		Name:    "foobar",
		Image:   "ubuntu/xenial",
		CpuNum:  1,
		RamSize: 256 * bytefmt.MEGABYTE,
	}

	assert.NoError(t, driver.Create(id, props))

	containers, err := driver.client.GetContainers()
	assert.NoError(t, err)

	exists := false
	for _, c := range containers {
		if c.Name == fmt.Sprintf("openvm-%v", id) {
			exists = true
		}
	}

	if !exists {
		t.Fatal("container does not exist")
	}

	assert.NoError(t, driver.Delete(id))

	containers, err = driver.client.GetContainers()
	assert.NoError(t, err)

	for _, c := range containers {
		if c.Name == fmt.Sprintf("openvm-%v", id) {
			t.Fatal("container still exists")
		}
	}
}
