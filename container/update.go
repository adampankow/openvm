package container

import (
	"fmt"

	"github.com/lxc/lxd/shared/api"
	"gitlab.com/openvm/openvm/model"
)

func (container *Container) Update(id int64, props model.MachineProperties) error {
	name := fmt.Sprintf("openvm-%v", id)

	status, _, err := container.Get(id)
	if err != nil {
		return err
	}

	/*if state.Name != "" && currentState.Name != state.Name {
		return errors.New("name change operation not supported")
	}*/

	if *status != props.Status {
		switch *status {
		case model.Running:
			put := api.ContainerStatePut{
				Action:  "start",
				Timeout: -1,
			}

			op, err := container.server.UpdateContainerState(name, put, "")
			if err != nil {
				return err
			}
			if err := op.Wait(); err != nil {
				return err
			}

		case model.Stopped:
			put := api.ContainerStatePut{
				Action:  "stop",
				Timeout: -1,
			}

			op, err := container.server.UpdateContainerState(name, put, "")
			if err != nil {
				return err
			}
			if err := op.Wait(); err != nil {
				return err
			}
		}
	}

	return nil
}
