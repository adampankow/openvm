package container

import (
	"fmt"

	"github.com/gorilla/websocket"
	lxd "github.com/lxc/lxd/client"
	"github.com/lxc/lxd/shared/api"
)

func (container *Container) Session(id int64, props SessionProperties) error {

	name := fmt.Sprintf("openvm-%v", id)

	ct, _, err := container.server.GetContainer(name)
	if err != nil {
		return err
	}

	if ct.StatusCode == api.Stopped {

		reqState := api.ContainerStatePut{
			Action:  "start",
			Timeout: -1,
		}

		op, err := container.server.UpdateContainerState(name, reqState, "")
		if err != nil {
			return err
		}

		err = op.Wait()
		if err != nil {
			return err
		}
	}

	req := api.ContainerExecPost{
		Command:     []string{"bash"},
		WaitForWS:   true,
		Interactive: true,
		Width:       props.Width,
		Height:      props.Height,
	}

	args := lxd.ContainerExecArgs{
		Stdin:  props.Stdin,
		Stdout: props.Stdout,
		Stderr: props.Stdout,
		Control: func(conn *websocket.Conn) {
			for msg := range props.Control {
				if err := conn.WriteJSON(msg); err != nil {
					return
				}
			}
		},
	}

	op, err := container.server.ExecContainer(name, req, &args)
	if err != nil {
		return err
	}

	go func() {
		err = op.Wait()
		if err != nil {
			panic(err) // FIXME
		}
	}()

	return nil
}
