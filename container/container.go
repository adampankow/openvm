package container

import (
	"io"
	"strings"

	lxd "github.com/lxc/lxd/client"
	"github.com/lxc/lxd/shared/api"
	"gitlab.com/openvm/openvm/config"
)

type Options struct {
	Enable      bool
	Remote      string
	ImageSource string
}

type Container struct {
	server      lxd.ContainerServer
	imageServer lxd.ImageServer
}

type SessionProperties struct {
	Stdin   io.ReadCloser
	Stdout  io.WriteCloser
	Control chan SessionControlMessage
	Width   int
	Height  int
}

type SessionControlMessage api.ContainerExecControl

func New(conf config.LxdConfig) (container *Container, err error) {
	var server lxd.ContainerServer
	var imageServer lxd.ImageServer

	if strings.HasPrefix(conf.Addr, "unix:") {
		server, err = lxd.ConnectLXDUnix(strings.TrimPrefix(strings.TrimPrefix(conf.Addr, "unix:"), "//"), nil)
		if err != nil {
			return
		}
	} else {
		server, err = lxd.ConnectLXD(conf.Addr, nil)
		if err != nil {
			return
		}
	}

	imageServer, err = lxd.ConnectSimpleStreams("https://images.linuxcontainers.org", nil)
	if err != nil {
		return
	}

	container = &Container{server: server, imageServer: imageServer}
	return
}
