package container

import (
	"fmt"

	"github.com/lxc/lxd/shared/api"
	"gitlab.com/openvm/openvm/model"
)

func (container *Container) Get(id int64) (*model.MachineStatus, *model.MachineStats, error) {
	name := fmt.Sprintf("openvm-%v", id)

	state, _, err := container.server.GetContainerState(name)
	if err != nil {
		return nil, nil, err
	}

	var status model.MachineStatus
	if state.StatusCode == api.Error || state.StatusCode == api.Stopped {
		status = model.Stopped
	} else {
		status = model.Running
	}

	stats := model.MachineStats{
		CpuUsage:    state.CPU.Usage,
		MemoryUsage: state.Memory.Usage,
		Processes:   state.Processes,
	}

	return &status, &stats, nil
}
