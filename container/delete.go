package container

import (
	"fmt"

	"github.com/lxc/lxd/shared/api"
)

func (container *Container) Delete(id int64) error {

	name := fmt.Sprintf("openvm-%v", id)

	ct, _, err := container.server.GetContainer(name)

	if err != nil {
		return err
	}

	if ct.StatusCode != 0 && ct.StatusCode != api.Stopped {

		req := api.ContainerStatePut{
			Action:  "stop",
			Timeout: -1,
			Force:   true,
		}

		op, err := container.server.UpdateContainerState(name, req, "")
		if err != nil {
			return err
		}

		err = op.Wait()
		if err != nil {
			return fmt.Errorf("Stopping the container failed: %s", err)
		}

		if ct.Ephemeral == true {
			return nil
		}
	}

	op, err := container.server.DeleteContainer(name)

	if err != nil {
		return err
	}

	return op.Wait()
}
