package model

import "github.com/lxc/lxd/shared/api"

type Image struct {
	Fingerprint string            `jsonapi:"primary,images"`
	Aliases     []api.ImageAlias  `jsonapi:"attr,aliases"`
	Properties  map[string]string `jsonapi:"attr,properties"`
}
