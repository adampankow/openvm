package config

import (
	"bytes"
	"io"
	"os"
	"path/filepath"

	"github.com/hashicorp/hcl"
)

type Config struct {
	Address string `hcl:"address"`

	LogLevel string `hcl:"log_level"`

	AllowOrigins []string `hcl:"allow_origins"`

	TLS TLSConfig `hcl:"tls"`

	JWT JwtConfig `hcl:"jwt"`

	Lxd LxdConfig `hcl:"lxd"`

	Postgres PostgresConfig `hcl:"postgres"`
}

type TLSConfig struct {
	Enable bool
	Auto   bool
	Cert   string
	Key    string
}

type LxdConfig struct {
	Addr string `hcl:"addr"`
}

type PostgresConfig struct {
	Address  string `hcl:"address"`
	Username string `hcl:"username"`
	Password string `hcl:"password"`
	Database string `hcl:"database"`
}

type JwtConfig struct {
	Secret string `hcl:"secret"`
}

func DefaultConfig() *Config {
	return &Config{
		Address:  ":7842",
		LogLevel: "DEBUG",
	}
}

func Parse(r io.Reader) (*Config, error) {
	var buf bytes.Buffer
	if _, err := io.Copy(&buf, r); err != nil {
		return nil, err
	}

	var config Config
	err := hcl.Decode(&config, buf.String())
	if err != nil {
		return nil, err
	}

	return &config, nil
}

func ParseFile(path string) (*Config, error) {
	path, err := filepath.Abs(path)
	if err != nil {
		return nil, err
	}

	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	config, err := Parse(f)
	if err != nil {
		return nil, err
	}

	return config, nil
}
