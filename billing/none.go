package billing

import "gitlab.com/openvm/openvm/model"

type NoneBilling struct {
}

func NewNoneBilling() (*NoneBilling, error) {
	return &NoneBilling{}, nil
}

func (a *NoneBilling) CreateUser(user model.User) error {
	return nil
}

func (a *NoneBilling) CreateMachine(machine model.Machine) error {
	return nil
}

func (a *NoneBilling) DeleteMachine(machine model.Machine) error {
	return nil
}
