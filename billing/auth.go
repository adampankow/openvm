package billing

import (
	"fmt"

	"gitlab.com/openvm/openvm/model"
)

type Billing interface {
	CreateUser(user model.User) error
	CreateMachine(machine model.Machine) error
	DeleteMachine(machine model.Machine) error
}

func NewBilling(name string) (Billing, error) {
	switch name {
	case "none":
		return NewNoneBilling()
	default:
		return nil, fmt.Errorf("unknown billing module '%s'", name)
	}
}
