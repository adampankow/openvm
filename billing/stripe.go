package billing

import (
	stripe "github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/customer"
	"gitlab.com/openvm/openvm/model"
)

type StripeBilling struct {
}

func NewStripeBilling() (*StripeBilling, error) {
	return &StripeBilling{}, nil
}

func (a *StripeBilling) CreateUser(user model.User) error {
	params := &stripe.CustomerParams{
		Email:  user.Email,
		Source: &stripe.SourceParams{Token: "TODO"},
	}
	_, err := customer.New(params)
	if err != nil {
		return err
	}
	// TODO user.StripeID = customer.ID
	return nil
}

func (a *StripeBilling) CreateMachine(machine model.Machine) error {
	return nil
}

func (a *StripeBilling) DeleteMachine(machine model.Machine) error {
	return nil
}

/*
import (
	"encoding/json"
	"net/http"

	stripe "github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/event"
	"github.com/stripe/stripe-go/sub"
	"gitlab.com/openvm/openvm/model"
	"go.uber.org/zap"
)

// Webhook recieves a Stripe event and performs the appropriate actions
func (h *Handler) Webhook(w http.ResponseWriter, r *http.Request) {
	var ev *stripe.Event
	if err := json.NewDecoder(r.Body).Decode(ev); err != nil {
		WriteInternalError(w, "webhook: event parse failed", err)
		return
	}

	// make sure the event is really from stripe
	ev, err := event.Get(ev.ID, nil)
	if err != nil {
		logger.Warn("webhook: stripe event fetch failure", zap.String("event id", ev.ID), zap.Error(err))
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	switch ev.Type {
	case "customer.deleted":
		if _, err := h.DB.Model(&model.User{}).Where("stripe_id = ?", ev.Account).Delete(); err != nil {
			WriteInternalError(w, "webhook: db error", err)
			return
		}
	case "customer.subscription.updated":
		var user model.User
		if err := h.DB.Model(&user).Where("stripe_id = ?", ev.Account).Select(); err != nil {
			WriteInternalError(w, "webhook: db error", err)
			return
		}
		subscription, err := sub.Get(user.StripeSubID, nil)
		if err != nil {
			WriteInternalError(w, "webhook: sub get error", err)
			return
		}
		user.Status = string(subscription.Status)
		if err := h.DB.Update(&user); err != nil {
			WriteInternalError(w, "webhook: db error", err)
			return
		}
	case "customer.subscription.deleted":
		var user model.User
		if err := h.DB.Model(&user).Where("stripe_id = ?", ev.Account).Select(); err != nil {
			WriteInternalError(w, "webhook: db error", err)
			return
		}
		user.Status = "" // TODO
		if err := h.DB.Update(&user); err != nil {
			WriteInternalError(w, "webhook: db error", err)
			return
		}
	case "invoice.payment_failed":
		// send email
	case "customer.subscription.trial_will_end":
		// send email
	case "customer.subscription.created":
		// send email
	}
}
*/
